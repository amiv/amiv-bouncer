/* Session and Users from AMIVAPI */

import m from 'mithril';
import ls from 'local-storage';
import { Button } from 'polythene-mithril';
import { apiUrl, OAuthId } from 'config';


// AMIVAPI OAUTH
// =============

// Persist token and state for oauth-request
let session = ls.get('session') || {};
ls.on('session', (newSession) => { session = newSession; m.redraw(); });

export function getToken() {
  return session.token;
}

// Redirect to OAuth landing page
export function login() {
  // Generate random state and reset currently stored session data
  const newSession = {
    state: Math.random().toString(),
  };
  ls.set('session', newSession);

  const query = m.buildQueryString({
    response_type: 'token',
    client_id: OAuthId,
    redirect_uri: window.location.origin,
    state: newSession.state,
  });

  // Redirect to AMIV api oauth page
  window.location.href = `${apiUrl}/oauth?${query}`;
}

let logoutMessage = '';
export function getLogoutMessage() { return logoutMessage; }

export function logout(message = '') {
  session = {};
  ls.set('session', session);
  logoutMessage = message;
}


// Extract token from query string automatically if state matches
const params = m.parseQueryString(window.location.search);
if (params.state && params.access_token && (params.state === session.state)) {
  session.token = params.access_token;
  ls.set('session', session);
  // Token is valid and stored in the session, so we can remove it from the URL
  window.history.replaceState({}, document.title, '/');
}


// AMIVAPI REQUESTS
// ================

let apiMessage = '';

/* Error if too many requests are attempted. */
function RequestError() {
  this.name = 'RequestError';
  this.message = 'Cannot send new requests, other requests are in progress!';
}


/* Projection to only include required fields for users. */
const proj = m.buildQueryString({
  projection: JSON.stringify({
    firstname: 1, lastname: 1, membership: 1, nethz: 1,
  }),
});

// User Interface
export const users = {
  // Request progress helpers
  totalRequests: 0,
  completedRequests: 0,
  get progress() {
    return this.totalRequests ? (this.completedRequests / this.totalRequests) : 0;
  },

  get busy() { return this.totalRequests !== this.completedRequests; },

  get list() { return Object.values(this.userdata); },

  // All users indexed by (unique nethz)
  userdata: {},

  /* Get ALL users in amivapi. Really, all of them. */
  async get() {
    if (this.busy) { throw new RequestError(); }
    // Reset Data
    this.userdata = {};

    // 1. Use first request to discover number of pages and check permissions
    const initialRequest = m.request({
      method: 'GET',
      url: `${apiUrl}/users?${proj}`,
      headers: {
        Authorization: session.token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    let response;
    try {
      response = await initialRequest;
      this.completedRequests += 1;
    } catch ({ _error: { code } }) {
      if (code === 401) {
        logout('You have been logged out because your session is invalid or ' +
          'has expired.');
      } else {
        logout('There is an error with the AMIV API, please try again and ' +
          'contact AMIV IT if the problem persists.');
      }
      return;
    }

    if (response._items.length === 0) {
      apiMessage = 'No users in API!';
      return;
    }
    // Check if patch possible. Randomly, we could get own user back
    // so check patch for everyone in first response to be sure
    function cannotPatch(user) {
      const { _links: { self: { methods } } } = user;
      return methods.indexOf('PATCH') === -1;
    }
    if (response._items.some(cannotPatch)) {
      logout('You have been logged out because your permissions are ' +
        'insufficient. You must be able to modify all users to ' +
        'use this tool.');
      return;
    }

    // 2. Process users in first response
    this.processResponse(response);

    // 3. Start requests for all other pages
    const { total: userCount, max_results: pageSize } = response._meta;
    const pages = Math.ceil(userCount / pageSize);

    this.totalRequests = pages;

    apiMessage = 'Requesting all users from API...';
    // We need to slow down requests
    try {
      for (let p = 2; p <= pages; p += 1) {
        // eslint-disable-next-line
        await this.getPage(p);
        this.completedRequests += 1;
      }
      apiMessage = `${this.list.length} users synchronized with API.`;
    } catch (err) {
      apiMessage = 'Some requests were unsuccessful, please reload page!';
    }
  },

  /* Get users on a specific page */
  async getPage(page) {
    const data = await m.request({
      method: 'GET',
      url: `${apiUrl}/users?${proj}&page=${page}`,
      headers: {
        Authorization: session.token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    this.processResponse(data);

    return data;
  },

  /* Helper to process response: add all users to internal table */
  processResponse(response) {
    response._items.forEach((user) => { this.userdata[user.nethz] = user; });
    return response;
  },

  /* Change membership of user to new value */
  async setMembership(userList, membership) {
    if (this.busy) { throw new RequestError(); }

    apiMessage = `Setting membership of ${userList.length} ` +
      `users to '${membership}...'`;

    try {
      // eslint-disable-next-line
      for (const { nethz } of userList) {
        const { _id: id, _etag: etag } = this.userdata[nethz];
        // eslint-disable-next-line
        const updates = await m.request({
          method: 'PATCH',
          url: `${apiUrl}/users/${id}`,
          headers: {
            Authorization: session.token,
            'If-Match': etag,
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: { membership },
        });
        this.userdata[nethz] = updates;
      }
      apiMessage = `${this.list.length} users synchronized with API.`;
    } catch (err) {
      apiMessage = 'Some requests were unsuccessful, please reload page!';
    }
  },

  async createUser(userList) {
    if (this.busy) { throw new RequestError(); }

    apiMessage = 'Creating user...';

    try {
      // eslint-disable-next-line
      for (const user of userList) {
        // eslint-disable-next-line
        const response = await m.request({
          method: 'POST',
          url: `${apiUrl}/usersync`,
          headers: {
            Authorization: session.token,
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: { nethz: user },
        });
        this.userdata[user] = response;
      }
      apiMessage = `${this.list.length} users synchronized with API.`;
    } catch (err) {
      apiMessage = 'Some requests were unsuccessful, please reload page!';
    }
  },
};


// Export API View
const statusView = {
  oninit() { users.get(); },
  view() {
    const progress = Math.round(users.progress * 100);
    return m('.header-api-status', [
      m(
        '.header-api-bar-container',
        m('.header-api-bar', { style: `width: ${progress}%` }, ''),
      ),
      m('.header-api-text', apiMessage),
    ]);
  },
};


const logoutView = {
  view() {
    return m('.header-api-logout', m(Button, {
      label: 'Logout',
      tone: 'dark',
      events: { onclick() { logout('Goodbye!'); } },
    }));
  },
};

// Export API View
export const apiView = {
  view() {
    return [
      m(statusView), m(logoutView),
    ];
  },
};
